const jwt = require("jsonwebtoken");
const secret = "productAPI";


// Token Creator
module.exports.createAccessToken = (user) => {
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret);
}

// Token Verification
module.exports.verify = (request, response, next) => {
	
	let token = request.headers.authorization
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send({
					auth: "Invalid token!"
				});
			}
			else{
                request.userData = data;
				console.log(request.userData)
				next();
			}
		})
	}
    else {
        response.send({message: "Authentication failed."})
    }
}

// Token decoder
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}
