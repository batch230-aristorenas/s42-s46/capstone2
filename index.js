const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

mongoose.connect("mongodb+srv://admin:admin@batch230.rc36bvq.mongodb.net/AristorenasECommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once("open", () => console.log("Now connected to Aristorenas-Mongo DB Atlas"));


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});