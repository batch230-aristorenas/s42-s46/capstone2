const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productControllers = require("../controllers/productControllers.js");


router.post("/create", auth.verify, productControllers.addProduct);

router.get("/active", productControllers.getActiveProducts);

router.get("/:productId", productControllers.getProduct);

router.put("/update/:productId", auth.verify, productControllers.updateProduct);

router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

router.delete("/delete/:productId", productControllers.deleteProduct);

router.get("/", auth.verify, productControllers.getAllProduct);


module.exports = router;