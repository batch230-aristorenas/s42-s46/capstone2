const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userControllers = require("../controllers/userControllers.js");


// router.post("/register", (request, response) => {
// 	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController))
// })

router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.post("/checkout", auth.verify, userControllers.userCheckout);

//router.get("/:userId", userControllers.getUser);


router.put("/makeAdmin/:userId", auth.verify, userControllers.makeAdmin);

router.get("/order/:userId", userControllers.getUserOrder);

router.delete("/delete/:userId", userControllers.deleteUser);

 //Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Route for user details
router.get("/details", auth.verify, userControllers.getProfile);

router.get("/", auth.verify, userControllers.getAllUser);

router.put("/update/:userId", auth.verify, userControllers.updateUser);




module.exports = router;