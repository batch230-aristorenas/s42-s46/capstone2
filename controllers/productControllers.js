const Product = require("../models/product.js");
const auth = require("../auth");


module.exports.addProduct = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    let newProduct = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        stock: request.body.stock
    });

    if(userData.isAdmin){
        newProduct.save((error, savedProduct) => {
            if(error){
                return console.log(error);
            }
            else {
                return response.status(201).send(true);
            }
        })
        // return newProduct.save()
        // .then(product => {
        //     console.log(product);
        //     return response.status(201).send("Thank you for registering a product.");
        // })
        // .catch(error => {
        //     console.log(error);
        //     response.send(false);
        // })
    }
    else {
		return response.status(401).send(false);
	};

}


module.exports.getActiveProducts = (request, response) => {
    return Product.find({isActive: true}).then(result => response.send(result));
}


module.exports.getProduct = (request, response) => {
    console.log(request.params.productId);

    return Product.findById(request.params.productId).then(result => response.send(result));
}


module.exports.updateProduct = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        
        // return updateProduct.save((error, updatedProduct) => {
        //     if(error) {
        //         return console.log(error);
        //     }
        //     else {
        //         let updateProduct = {
        //             name: request.body.name,
        //             description: request.body.description,
        //             price: request.body.price,
        //             stock: request.body.stock
        //             };
        //         return Product.findByIdAndUpdate(request.params.productId, updateProduct, {new:true})
        //         .then(result => {
        //             console.log(result);
        //             response.send(result);
        //             })
        //     }
        // })
        let updateProduct = {
            name: request.body.name,
            description: request.body.description,
            price: request.body.price,
            stock: request.body.stock
        }

        return Product.findByIdAndUpdate(request.params.productId, updateProduct, {new:true})
        .then(result => {
            console.log(result);
            response.send(true);
        })
        .catch(error => {
            console.log(error);
            response.send(false);
        });
    }
    else {
        return response.status(404).send(false);
    }
}


module.exports.archiveProduct = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        let updateIsActiveField = {
            isActive: request.body.isActive
        }
        if(userData.isAdmin){
            return Product.findByIdAndUpdate(request.params.productId, updateIsActiveField)
            .then(result => {
                console.log(result);
                response.send(true);
            })
            .catch(error => {
                console.log(error);
                response.send(false);
            })
        }
        
        }
        else{
            return response.status(404).send("Access denied!");
    }
    
}


module.exports.deleteProduct = (request, response) => {
    console.log(request.params.productId);

    return Product.findByIdAndDelete(request.params.productId).then(result => response.send(result));
}


/*module.exports.getAllProduct = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    return Product.find({}).then(result => response.send(result))
}
*/

module.exports.getAllProduct = (req, res) =>{
    
    // const userData = auth.decode(req.headers.authorization);

    if(req.userData.isAdmin){
        return Product.find({}).then(result => res.send(result));
    }
    else{
        return res.send(false);
        // return res.status(401).send("You don't have access to this page!");
    }
}