const bcrypt = require("bcrypt");
const { request } = require("express");
const auth = require("../auth.js");

const Product = require("../models/product.js");
const User = require("../models/user.js");

module.exports.registerUser = (request, response) => {
    // User.findOne({
    //     $or: [{email: request.body.email},
    //           {mobileNo: request.body.mobileNo}]
    // }).then(result => {
    //     console.log(result);
    //     if(result != null && result.email == request.body.email){
    //         return response.send("Email already exist.")
    //     }
    //     else if(result != null && result.mobileNo == request.body.mobileNo){
    //         return response.send("Mobile No. already exist.")
    //     }
    //     else {
            let newUser = new User({
                firstName: request.body.firstName,
                lastName: request.body.lastName,
                email: request.body.email,
                password: bcrypt.hashSync(request.body.password, 10),
                mobileNo: request.body.mobileNo
            })
            return newUser.save()
                .then(user => {
                        console.log(user);
                        // send "true" if the user is created
                        response.send(true);
                    })
                    .catch(error =>{
                        console.log(error);
                        // send "false" if any error is encountered.
                        res.send(false);
                    })
    }



    module.exports.loginUser = (request, response) => {
        return User.findOne({email: request.body.email})
        .then(result => {
            if(result == null){
                return response.send({message: "No user found."});
            }
            else {
                const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

                if(isPasswordCorrect){
                    return response.send({accessToken: auth.createAccessToken(result)});
                }
                else {
                    return response.send({message: "Incorrect password!"});
                }
            }
        })
    }


    module.exports.userCheckout  = async (request, response) =>{

        const userData = auth.decode(request.headers.authorization);

        let productName = await Product.findById(request.body.productId)
        .then(result => result.name);

        let data = {
            userId :userData.id,
            email : userData.email,
            productId : request.body.productId,
            productName : productName
        }
    

    console.log(data);

    let isUserUpdated = await User.findById(data.userId)
    .then(user => {
        user.orders.push({
            products : {
                productId : data.productId,
                productName : data.productName,
                quantity : data.quantity
            }
        });

        return user.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    })

    console.log(isUserUpdated);

    let isProductUpdated = await Product.findById(data.productId)
    .then(product => {
        product.orders.push({
            userId : data.userId,
            email : data.email,
            products: {
                productId: data.productId,
                productName: data.productName,
                quantity: data.quantity
            }
        });

        product.stock -= 1;
            if(product.stock <= 0){
                console.log("SOLD OUT!");
            }
            else{
                return product.save()
                .then(result => {
                    console.log(result);
                return true;
            })
            }
    })
    .catch(error => {
        console.log(error);
        return false;
    })
    console.log(isProductUpdated);

    (isUserUpdated == true && isProductUpdated == true)? response.send(true): response.send(false);
}


// module.exports.getUser = (request, response) => {
//     console.log(request.params.userId);

//     return User.findById(request.params.userId).then(result => response.send(result));
// }


// Stretch Goals
module.exports.makeAdmin = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        let updateIsAdminField = {
            isAdmin: request.body.isAdmin
        }
        if(userData.isAdmin){
            return User.findByIdAndUpdate(request.params.userId, updateIsAdminField)
            .then(result => {
                console.log(result);
                response.send(true);
            })
            .catch(error =>{
                console.log(error);
                response.send(false);
            })
        }
        
    
    }
    else{
        return response.status(404).send("Access denied!");
    }
}


module.exports.getUserOrder = (request, response) => {
    console.log(request.params.userId);

    return User.findById(request.params.userId).then(result => response.send(result.orders));
}


module.exports.deleteUser = (request, response) => {
    console.log(request.params.userId);

    return User.findByIdAndDelete(request.params.userId).then(result => response.send(result));
}

module.exports.checkEmailExists = (req, res) =>{
    return User.find({email: req.body.email}).then(result =>{

        // The result of the find() method returns an array of objects.
             // we can use array.length method for checking the current result length
        console.log(result);

        // The user already exists
        if(result.length > 0){
            return res.send(true);
            // return res.send("User already exists!");
        }
        // There are no duplicate found.
        else{
            return  res.send(false);
            // return res.send("No duplicate found!");
        }
    })
    .catch(error => res.send(error));
}

module.exports.getProfile = (req, res) => {
        
        const userData = auth.decode(req.headers.authorization);

        console.log(userData);

        return User.findById(userData.id).then(result =>{
            result.password = "***";
            res.send(result);
        })
    }

module.exports.getAllUser = (req, res) =>{
    
    // const userData = auth.decode(req.headers.authorization);

    if(req.userData.isAdmin){
        return User.find({}).then(result => res.send(result));
    }
    else{
        return res.send(false);
        // return res.status(401).send("You don't have access to this page!");
    }
}


module.exports.updateUser = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        
        let updateUser = {
            email: request.body.email,
            mobileNo: request.body.mobileNo,
        }

        return User.findByIdAndUpdate(request.params.userId, updateUser, {new:true})
        .then(result => {
            console.log(result);
            response.send(result);
        })
        .catch(error => {
            console.log(error);
            response.send(error);
        });
    }
    else {
        return response.status(404).send(false);
    }
}


