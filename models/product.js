const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    // _id: {
    //     type: String,
    // },
    name: {
        type: String,
        required: [true, "Product name is required."]
    },
    description: {
        type: String,
        required: [true, "Description is required."]
    },
    price: {
        type: Number,
        required: [true, "Product price is required."]
    },
    stock: {
        type: Number,
        required: [true, "Number of stocks is required."]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [{
        userId: {
            type: String
        },
        userEmail: {
            type: String
        },
        quantity: {
            type: Number
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        }
    }]
})

module.exports = mongoose.model("Product", productSchema);